package com.wanglei.swaggergentest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerGenTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerGenTestApplication.class, args);
    }

}
